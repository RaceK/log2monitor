package cn.thinking.log2monitor;

import cn.thinking.log2monitor.aop.OperationLogOpsTransformer;
import cn.thinking.log2monitor.configuration.LogMonitorConfiguration;
import cn.thinking.log2monitor.model.LogOpsModel;
import cn.thinking.log2monitor.service.TestService;
import com.alibaba.fastjson2.JSON;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.expression.Expression;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.test.context.ContextConfiguration;

import java.lang.reflect.Method;
import java.util.Collection;

/**
 * 测试类启动时只会默认扫描和注入启动类所在包以及其子包下面定义的组件，
 * 或者使用ComponentScan自定义扫描包路径。
 * 只有被springboot自动加载注入的切面，切面才会生效。
 *
 * @author Long.F
 * @date 2022/12/27 9:34
 */
@Slf4j
@SpringBootTest
@ContextConfiguration(classes = {
        TestService.class, LogMonitorConfiguration.class
})
@EnableAspectJAutoProxy(proxyTargetClass = true)
class Log2MonitorApplicationTests {

    @Autowired
    TestService testService;

    @Autowired
    OperationLogOpsTransformer operationLogParser;

    @Test
    void contextLoads() {
        testService.testBizId("10086");
        testService.testMultiBizId("10089");
        try {
            testService.testErrorMsg("10090");
        } catch (Exception ignored) {
        }
        testService.testGenericType("bizId3");
    }

    @Test
    void testSpElParse(){
        SpelExpressionParser parser = new SpelExpressionParser();
        Expression expression = parser.parseExpression("2*2");
        log.info((String) expression.getValue());
    }


    @Test
    void testFunctionParse(){
        testService.testFunctionParse("testFunctionParseBizId");
    }

    @Test
    void testAnnotationParse(){
        Class<TestService> specificClass = TestService.class;
        Method[] methods = specificClass.getMethods();
        for (Method method : methods) {
            if (!method.getName().startsWith("test")) {
                continue;
            }
            Collection<LogOpsModel> logOpsModels = operationLogParser.scanMethodAndGenerateOpsModel(method, specificClass);
            log.info(JSON.toJSONString(logOpsModels));
        }
    }

}
