package cn.thinking.log2monitor.service;

import cn.thinking.log2monitor.annotation.OperationLog;
import cn.thinking.log2monitor.annotation.OperationLogs;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.test.context.TestComponent;

/**
 * @author Long.F
 * @date 2022/12/27 9:34
 */
@Slf4j
@TestComponent
public class TestService {

    @OperationLog(bizId = "#bizId", bizType = "'testBizId'", successMsg = "'执行成功'")
    public void testBizId(String bizId) {
        log.info("### testBizId " + bizId);
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        log.info("执行结束");
    }

    @OperationLogs(value = {
            @OperationLog(bizId = "#bizId", bizType = "'testMultiBizId1'", successMsg = "'执行成功'"),
            @OperationLog(bizId = "2", bizType = "'testMultiBizId2'", successMsg = "'执行成功'")
    })
    public void testMultiBizId(String bizId){
        log.info("###执行 testMultiBizId " + bizId);
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        log.info("执行结束");
    }

    @OperationLog(bizId = "#bizId", bizType = "'testErrorMsg'",
            successMsg = "'执行成功'",
            failureMsg = "'执行失败'")
    public void testErrorMsg(String bizId){
        log.info("###执行 testErrorMsg " + bizId);
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        throw new IllegalArgumentException("测试方法内出现异常并抛出");
    }

    @OperationLog(bizId = "#bizId", bizType = "'testGenericType'",
            successMsg = "成功")
    public <T> void testGenericType(T bizId){
        log.info("###执行 testErrorMsg " + bizId.getClass().toString());
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @OperationLog(bizId = "{{#bizId}}", bizType = "testFunctionParse",
            successMsg = "{DEFAULT{#bizId}}")
    public void testFunctionParse(String bizId) {
        log.info("###执行 testFunctionParse " + bizId);
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
