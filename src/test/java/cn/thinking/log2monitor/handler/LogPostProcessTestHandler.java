package cn.thinking.log2monitor.handler;

import cn.thinking.log2monitor.model.LogDetailModel;
import cn.thinking.log2monitor.service.IOperationLogPostProcessHandler;
import com.alibaba.fastjson2.JSON;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.test.context.TestComponent;

/**
 * @author Long.F
 * @date 2022/12/28 18:19
 */
@Slf4j
@TestComponent
public class LogPostProcessTestHandler implements IOperationLogPostProcessHandler {
    @Override
    public void handle(LogDetailModel logDetailModel) {
        log.info("{} - {}", "LogPostProcessTestHandler##handle",JSON.toJSONString(logDetailModel));
    }
}
