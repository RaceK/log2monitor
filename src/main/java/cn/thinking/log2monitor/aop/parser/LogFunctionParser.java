package cn.thinking.log2monitor.aop.parser;

import cn.thinking.log2monitor.service.ILogFunctionParseService;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.util.Strings;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.Objects;

/**
 * 日志模板中的方法解析器
 *
 * @author Long.F
 * @date 2023/1/4 15:34
 */
@Component
public class LogFunctionParser {

    ILogFunctionParseService functionParseService;

    public LogFunctionParser(ILogFunctionParseService functionParseService) {
        this.functionParseService = functionParseService;
    }

    public String getFunctionReturnValue(Map<String, String> functionNameAndResMap,
                                         String expression, String functionName, Object functionArgs) {
        if (StringUtils.isEmpty(functionName)) {
            return Objects.isNull(functionArgs) ? Strings.EMPTY : functionArgs.toString();
        }
        String functionResult;
        String functionCallInstanceKey = getFunctionInstanceCalledKey(functionName, expression);
        if (Objects.nonNull(functionNameAndResMap) && functionNameAndResMap.containsKey(functionCallInstanceKey)) {
            functionResult = functionNameAndResMap.get(functionCallInstanceKey);
        } else {
            functionResult = functionParseService.applyFunction(functionName, functionArgs);
        }
        return functionResult;
    }


    /**
     * 函数执行之前将模板中的调用替换成函数执行后的的结果，此时函数调用的唯一标志：函数名+参数表达式
     * 即函数执行结果缓存池中的Key
     *
     * @param functionName    函数名称
     * @param paramExpression 解析前的表达式
     * @return 函数缓存的key
     */
    public String getFunctionInstanceCalledKey(String functionName, String paramExpression) {
        return functionName + paramExpression;
    }

    /**
     * 判断函数是否属于日志解析函数
     */
    public boolean isLogFunction(String functionName) {
        return functionParseService.isLogFunction(functionName);
    }


//    @Autowired
//    public void setFunctionParseService(ILogFunctionParseService functionParseService) {
//        this.functionParseService = functionParseService;
//    }
}
