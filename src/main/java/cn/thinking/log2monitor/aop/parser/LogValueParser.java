package cn.thinking.log2monitor.aop.parser;

import cn.thinking.log2monitor.aop.OperationLogExpressionEvaluator;
import cn.thinking.log2monitor.model.MethodExecuteResultModel;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.expression.AnnotatedElementKey;
import org.springframework.expression.EvaluationContext;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 日志模板中的值解析器
 *
 * @author Long.F
 * @date 2023/1/4 15:38
 */
@Component
public class LogValueParser implements BeanFactoryAware {

    private BeanFactory beanFactory;

    private final OperationLogExpressionEvaluator expressionEvaluator = new OperationLogExpressionEvaluator();
    private static final Pattern EXPRESSION_PLACE_HOLDER_PATTERN = Pattern.compile("\\{\\s*(\\w*)\\s*\\{(.*?)}}");
    public static final String COMMA = ",";

    LogFunctionParser functionParser;


    public String singleProcessTemplate(String template, MethodExecuteResultModel executeResult, Map<String, String> functionNameAndReturnMap) {
        Map<String, String> expressionValue = processTemplate(List.of(template), executeResult, functionNameAndReturnMap);
        return expressionValue.get(template);
    }


    public Map<String, String> processTemplate(Collection<String> templates, MethodExecuteResultModel executeResult,
                                               Map<String, String> functionNameAndReturnMap) {
        Map<String, String> expressionValues = new HashMap<>(templates.size());
        EvaluationContext evaluationContext = expressionEvaluator.createEvaluationContext(executeResult.getMethod(), executeResult.getArgs(),
                executeResult.getTargetClass(), executeResult.getResult(), executeResult.getErrorMsg(), beanFactory);

        for (String template : templates) {
            // 表达式不存在占位符
            if (!template.contains("{")) {
                expressionValues.put(template, template);
                continue;
            }
            // 表达式存在占位符
            StringBuilder parsedStr = new StringBuilder();
            Matcher matcher = EXPRESSION_PLACE_HOLDER_PATTERN.matcher(template);
            AnnotatedElementKey annotatedElementKey = new AnnotatedElementKey(executeResult.getMethod(), executeResult.getTargetClass());
            while(matcher.find()) {
                String logFunctionName = matcher.group(1);
                String expression = matcher.group(2);
                // 表达式解析
                Object expressionArgs = expressionEvaluator.parseExpression(expression, annotatedElementKey, evaluationContext);
                expression = functionParser.getFunctionReturnValue(functionNameAndReturnMap, expression, logFunctionName, expressionArgs);
                matcher.appendReplacement(parsedStr, Matcher.quoteReplacement(StringUtils.defaultString(expression)));
            }
            matcher.appendTail(parsedStr);
            expressionValues.put(template, parsedStr.toString());
        }
        return expressionValues;
    }

    @Override
    public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
        this.beanFactory = beanFactory;
    }

    @Autowired
    public void setFunctionParser(LogFunctionParser functionParser) {
        this.functionParser = functionParser;
    }
}
