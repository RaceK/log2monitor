package cn.thinking.log2monitor.aop;

import cn.thinking.log2monitor.annotation.OperationLog;
import cn.thinking.log2monitor.annotation.OperationLogs;
import cn.thinking.log2monitor.model.LogOpsModel;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.BridgeMethodResolver;
import org.springframework.core.annotation.AnnotatedElementUtils;
import org.springframework.stereotype.Component;
import org.springframework.util.ClassUtils;
import org.springframework.util.CollectionUtils;

import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Long.F
 * @date 2022/12/30 9:35
 */
@Slf4j
@Component
public class OperationLogOpsTransformer {

    /**
     * 扫描方法，解析注解并将注解内容包装成 {@link LogOpsModel}
     *
     * @param method      目标方法
     * @param targetClass 目标方法所属的Class对象
     * @return LogOpsModel 实体类
     */
    public Collection<LogOpsModel> scanMethodAndGenerateOpsModel(Method method, Class<?> targetClass) {
        // 仅处理 public 修饰符的方法
        if (!Modifier.isPublic(method.getModifiers())) {
            return Collections.emptyList();
        }

        // 方法可能在接口上，但我们需要目标类的属性；如果目标类为空，则该方法将保持不变
        Method specificMethod = ClassUtils.getMostSpecificMethod(method, targetClass);
        // 如果我们正在处理带有泛型参数的方法，则需要找到原始方法。
        specificMethod = BridgeMethodResolver.findBridgedMethod(specificMethod);

        // 解析注解
        Set<LogOpsModel> res = new HashSet<>();
        // 从具体实现的方法中解析
        Collection<LogOpsModel> operationLogAnnotations = parseOperationLogAnnotation(specificMethod);
        Collection<LogOpsModel> multiOperationLogAnnotations = parseMultiOperationLogAnnotation(specificMethod);
        // 从抽象的方法中解析
        Collection<LogOpsModel> abstractOperationLogAnnotations
                = parseOperationLogAnnotation(ClassUtils.getInterfaceMethodIfPossible(method, targetClass));
        Collection<LogOpsModel> abstractMultiOperationLogAnnotations
                = parseMultiOperationLogAnnotation(ClassUtils.getInterfaceMethodIfPossible(method, targetClass));

        res.addAll(operationLogAnnotations);
        res.addAll(multiOperationLogAnnotations);
        res.addAll(abstractOperationLogAnnotations);
        res.addAll(abstractMultiOperationLogAnnotations);
        return res;
    }


    /**
     * 解析注解 {@link OperationLog}
     *
     * @param ae 目标方法
     * @return 解析后的实体类集合
     */
    private Collection<LogOpsModel> parseOperationLogAnnotation(AnnotatedElement ae) {
        Collection<OperationLog> operationLogAnnotations = AnnotatedElementUtils.findAllMergedAnnotations(ae, OperationLog.class);
        Collection<LogOpsModel> res = new ArrayList<>();
        if (CollectionUtils.isEmpty(operationLogAnnotations)) {
            return res;
        }

        for (OperationLog annotation : operationLogAnnotations) {
            res.add(buildFromAnnotation(ae, annotation));
        }
        return res;
    }


    /**
     * 解析注解 {@link OperationLogs}
     *
     * @param ae 目标方法
     * @return 解析后的实体类集合
     */
    private Collection<LogOpsModel> parseMultiOperationLogAnnotation(AnnotatedElement ae) {
        Collection<OperationLogs> operationLogAnnotations = AnnotatedElementUtils.findAllMergedAnnotations(ae, OperationLogs.class);
        Collection<LogOpsModel> res = new ArrayList<>();
        if (CollectionUtils.isEmpty(operationLogAnnotations)) {
            return res;
        }

        operationLogAnnotations.stream()
                .filter(l -> ArrayUtils.isNotEmpty(l.value()))
                .forEach(l -> {
                    for (OperationLog annotation : l.value()) {
                        res.add(buildFromAnnotation(ae, annotation));
                    }
                });
        return res;
    }


    private LogOpsModel buildFromAnnotation(AnnotatedElement ae, OperationLog annotation) {
        LogOpsModel model = LogOpsModel.builder()
                .successLogTpl(annotation.successMsg())
                .failureLogTpl(annotation.failureMsg())
                .bizIdTpl(annotation.bizId())
                .bizTypeTpl(annotation.bizType())
                .bizSubTypeTpl(annotation.bizSubType())
                .operatorIdTpl(annotation.operatorId())
                .tagTpl(annotation.tag())
                .extraTpl(annotation.extra())
                .recordConditionTpl(annotation.recordCondition())
                .recordSuccessCondition(annotation.recordSuccessCondition())
                .customizeSuccessTpl(annotation.customizeSuccess())
                .build();
        validateLogOpsModel(model, ae);
        return model;
    }


    private void validateLogOpsModel(LogOpsModel model, AnnotatedElement ae) {
        if (StringUtils.isAllEmpty(model.getSuccessLogTpl(), model.getFailureLogTpl())) {
            throw new IllegalArgumentException(String.format("在 %s 上存在非法的注解配置, 注解需配置至少一个非空的成功消息模板或失败消息模板", ae.toString()));
        }
    }

}
