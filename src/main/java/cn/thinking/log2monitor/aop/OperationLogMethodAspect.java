package cn.thinking.log2monitor.aop;

import cn.thinking.log2monitor.annotation.OperationLog;
import cn.thinking.log2monitor.aop.parser.LogFunctionParser;
import cn.thinking.log2monitor.aop.parser.LogValueParser;
import cn.thinking.log2monitor.context.LogRecordContext;
import cn.thinking.log2monitor.model.LogDetailModel;
import cn.thinking.log2monitor.model.LogOpsModel;
import cn.thinking.log2monitor.model.MethodExecuteResultModel;
import cn.thinking.log2monitor.model.OperationLogRecordModel;
import cn.thinking.log2monitor.service.ILogPerformanceMonitor;
import cn.thinking.log2monitor.service.IOperationLogPostProcessHandler;
import cn.thinking.log2monitor.service.IOperationLogRecordService;
import cn.thinking.log2monitor.service.IOperatorIdGetService;
import cn.thinking.log2monitor.service.LogRecordErrorHandler;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.aop.framework.AopProxyUtils;
import org.springframework.aop.support.AopUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.expression.AnnotatedElementKey;
import org.springframework.expression.EvaluationContext;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StopWatch;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static cn.thinking.log2monitor.service.ILogPerformanceMonitor.MONITOR_NAME;
import static cn.thinking.log2monitor.service.ILogPerformanceMonitor.MONITOR_TASK_AFTER_EXECUTE;
import static cn.thinking.log2monitor.service.ILogPerformanceMonitor.MONITOR_TASK_BEFORE_EXECUTE;
import static cn.thinking.log2monitor.service.ILogPerformanceMonitor.MONITOR_TASK_EXECUTE;

/**
 * 日志处理切面
 *
 * @author Long.F
 * @date 2022/12/27 9:45
 */
@Slf4j
@Aspect
@Component
public class OperationLogMethodAspect implements BeanFactoryAware {

    /**
     * 表达式中函数调用的正则表达式匹配符
     */
    private static final Pattern FUNCTION_CALLED_EXPRESSION_PATTERN = Pattern.compile("\\{\\s*(\\w*)\\s*\\{(.*?)}}");
    public static final String COMMA = ",";

    OperationLogExpressionEvaluator expressionEvaluator;
    LogFunctionParser functionParser;
    LogValueParser valueParser;
    OperationLogOpsTransformer operationLogParser;
    ILogPerformanceMonitor performanceMonitor;

    IOperationLogPostProcessHandler logPostProcessHandler;
    LogRecordErrorHandler logRecordErrorHandler;
    IOperatorIdGetService operatorIdGetService;
    IOperationLogRecordService logRecordService;


    BeanFactory beanFactory;

    public OperationLogMethodAspect(LogFunctionParser functionParser, LogValueParser valueParser,
                                    OperationLogOpsTransformer operationLogParser, ILogPerformanceMonitor performanceMonitor) {
        this.functionParser = functionParser;
        this.valueParser = valueParser;
        this.operationLogParser = operationLogParser;
        this.performanceMonitor = performanceMonitor;
        this.expressionEvaluator = new OperationLogExpressionEvaluator();
    }

    /**
     * 切点
     */
    @Pointcut("@annotation(cn.thinking.log2monitor.annotation.OperationLog) || @annotation(cn.thinking.log2monitor.annotation.OperationLogs)")
    private void pointCut() {
    }

    /**
     * 连接点织入处理
     *
     * @param pjp 连接点
     * @return 被代理方法执行的结果
     * @throws Throwable 被代理方法执行出现的异常
     */
    @Around("pointCut()")
    public Object doAround(ProceedingJoinPoint pjp) throws Throwable {
        // 如果是代理方法，则不拦截
        if (AopUtils.isAopProxy(pjp.getTarget())) {
            return pjp.proceed();
        }

        Object result = null;
        StopWatch stopWatch = new StopWatch(MONITOR_NAME);

        // 验证添加了注解的方法，验证失败则不执行日志切面处理逻辑
        Method method;
        Class<?> targetClass;
        try {
            method = extractMethod(pjp);
            targetClass = getTargetClass(pjp);
        } catch (Throwable throwable) {
            return pjp.proceed();
        }

        // 方法执行前
        Collection<LogOpsModel> operations = new HashSet<>();
        Map<String, String> functionNameAndReturnMap = new HashMap<>(16);
        stopWatch.start(MONITOR_TASK_BEFORE_EXECUTE);
        // 上下文变量初始化
        LogRecordContext.putEmptyPlaceholder();
        try {
            // 解析注解 -> 获取消息模板 -> 将前期需要填入的信息先填入
            operations = operationLogParser.scanMethodAndGenerateOpsModel(method, targetClass);
            List<String> templateBeforeExec = getTemplateNeedProcessBeforeFunctionExecute(operations);
            functionNameAndReturnMap = processTemplatesBeforeTargetMethodExecute(templateBeforeExec, pjp.getArgs(), targetClass, method);
        } catch (Throwable throwableBeforeFunc) {
            log.error("原方法执行前 执行日志切面逻辑 出现异常 : ", throwableBeforeFunc);
        } finally {
            stopWatch.stop();
        }

        // 执行被代理方法,记录方法执行结果
        MethodExecuteResultModel executeResult = new MethodExecuteResultModel(method, pjp.getArgs(), pjp.getClass());
        stopWatch.start(MONITOR_TASK_EXECUTE);
        try {
            result = pjp.proceed();
            executeResult.setSuccess(true);
            executeResult.setResult(result);
        } catch (Throwable functionThrowable) {
            executeResult.setSuccess(false);
            executeResult.setThrowable(functionThrowable);
        } finally {
            stopWatch.stop();
            executeResult.setExecuteCost(stopWatch.getLastTaskTimeMillis());
        }

        // 方法执行后
        stopWatch.start(MONITOR_TASK_AFTER_EXECUTE);
        try {
            handleAfterTargetMethodExecute(operations, executeResult, functionNameAndReturnMap);
        } catch (Throwable throwableAfterExecution) {
            log.error("原方法执行后 执行日志切面逻辑 出现异常 : ", throwableAfterExecution);
        } finally {
            try {
                LogRecordContext.clear();
                stopWatch.stop();
                try {
                    performanceMonitor.output(stopWatch);
                } catch (Throwable throwable) {
                    log.error("输出性能监视出现异常 : ", throwable);
                }
            } catch (Throwable throwableFinally) {
                log.error("切面执行最终后置处理逻辑 出现异常 : ", throwableFinally);
            }
        }

        // 如果原方法执行有异常，则抛出
        if (Objects.nonNull(executeResult.getThrowable())) {
            throw executeResult.getThrowable();
        }

        return result;
    }


    /**
     * 获取需要在方法执行之前解析的模板
     */
    private List<String> getTemplateNeedProcessBeforeFunctionExecute(Collection<LogOpsModel> operations) {
        List<String> res = new ArrayList<>();
        for (LogOpsModel operation : operations) {
            List<String> templates = getSpElTemplates(operation, operation.getSuccessLogTpl());
            if (CollectionUtils.isEmpty(templates)) {
                continue;
            }
            res.addAll(templates);
        }
        return res;
    }


    /**
     * 从注解中获取模板
     */
    private List<String> getSpElTemplates(LogOpsModel operation, String... actions) {
        List<String> spElTemplates = new ArrayList<>();
        spElTemplates.add(operation.getBizIdTpl());
        spElTemplates.add(operation.getBizTypeTpl());
        spElTemplates.add(operation.getBizSubTypeTpl());
        spElTemplates.add(operation.getOperatorIdTpl());
        spElTemplates.add(operation.getTagTpl());
        spElTemplates.add(operation.getExtraTpl());
        spElTemplates.add(operation.getRecordConditionTpl());
        spElTemplates.add(operation.getCustomizeSuccessTpl());
        spElTemplates.addAll(Arrays.asList(actions));
        // 过滤掉空字符串和Null
        return spElTemplates.stream().filter(StringUtils::isNotBlank).collect(Collectors.toList());
    }

    /**
     * 在方法执行之前先预处理一遍模板，如果模板中出现了函数，则先执行函数，并缓存函数执行结果
     *
     * @param templates    模板
     * @param args         方法参数
     * @param targetClass  目标类
     * @param targetMethod 目标方法
     * @return 方法名以及方法执行后的返回值
     */
    private Map<String, String> processTemplatesBeforeTargetMethodExecute(List<String> templates, Object[] args, Class<?> targetClass, Method targetMethod) {
        Map<String, String> functionNameAndReturnValueMap = new HashMap<>(16);
        EvaluationContext evaluationContext = expressionEvaluator.createEvaluationContext(targetMethod, args, targetClass, null, null, beanFactory);
        // 扫描模板
        for (String expressionTemplate : templates) {
            if (expressionTemplate.contains("{")) {
                // 匹配表达式
                Matcher matcher = FUNCTION_CALLED_EXPRESSION_PATTERN.matcher(expressionTemplate);
                while (matcher.find()) {
                    String expression = matcher.group(2);
                    if (StringUtils.containsAny(expression, "_return", "_errorMsg")) {
                        continue;
                    }
                    AnnotatedElementKey annotatedElementKey = new AnnotatedElementKey(targetMethod, targetClass);
                    String functionName = matcher.group(1);
                    // 如果是方法则解析表达式并填充进模板中
                    if (functionParser.isLogFunction(functionName)) {
                        Object value = expressionEvaluator.parseExpression(expression, annotatedElementKey, evaluationContext);
                        String functionReturnValue = functionParser.getFunctionReturnValue(functionNameAndReturnValueMap, expression, functionName, value);
                        String functionInstanceCalledKey = functionParser.getFunctionInstanceCalledKey(functionName, expression);
                        functionNameAndReturnValueMap.put(functionInstanceCalledKey, functionReturnValue);
                    }
                }
            }
        }
        return functionNameAndReturnValueMap;
    }


    /**
     * 在方法执行后执行切面逻辑
     */
    private void handleAfterTargetMethodExecute(Collection<LogOpsModel> operations, MethodExecuteResultModel executeResult, Map<String, String> functionNameAndReturnMap) {
        if (CollectionUtils.isEmpty(operations)) {
            return;
        }

        for (LogOpsModel operation : operations) {
            if (StringUtils.isAllBlank(operation.getSuccessLogTpl(), operation.getFailureLogTpl())) {
                continue;
            }
            if (validateLogRecordCondition(operation, executeResult, functionNameAndReturnMap)) {
                continue;
            }
            if (executeResult.isSuccess()) {
                applySuccessResult(operation, executeResult, functionNameAndReturnMap);
            } else {
                applyErrorResult(operation, executeResult, functionNameAndReturnMap);
            }
        }

    }

    /**
     * 校验日志记录的条件表达式
     *
     * @return 返回是否需要记录日志
     */
    private boolean validateLogRecordCondition(LogOpsModel operation, MethodExecuteResultModel executeResult, Map<String, String> functionNameAndReturnMap) {
        String recordConditionTpl = operation.getRecordConditionTpl();
        if (StringUtils.isBlank(recordConditionTpl)) {
            return false;
        }
        String recordRes = valueParser.singleProcessTemplate(recordConditionTpl, executeResult, functionNameAndReturnMap);
        return StringUtils.endsWithIgnoreCase(recordRes, "false");
    }


    private void applySuccessResult(LogOpsModel operation, MethodExecuteResultModel executeResult, Map<String, String> functionNameAndReturnMap) {
        String actionLog = operation.getSuccessLogTpl();
        boolean isSuccess = true;
        String recordSuccessCondition = operation.getRecordSuccessCondition();
        // 判断记录的是操作成功的日志还是操作失败的日志
        if (StringUtils.isNotBlank(recordSuccessCondition)) {
            String recordCondition = valueParser.singleProcessTemplate(recordSuccessCondition, executeResult, functionNameAndReturnMap);
            if (StringUtils.endsWithIgnoreCase(recordCondition, "false")) {
                actionLog = operation.getFailureLogTpl();
                isSuccess = false;
            }
        }
        if (StringUtils.isBlank(actionLog)) {
            return;
        }
        // 获取全部表达式模板并解析
        List<String> templates = getSpElTemplates(operation, actionLog);
        String operatorId = getOperatorIdFromService(operation, templates);
        Map<String, String> expressionValues = valueParser.processTemplate(templates, executeResult, functionNameAndReturnMap);
        // 日志持久化操作
        saveLog(expressionValues, operation, operatorId, actionLog, isSuccess);
    }


    private void applyErrorResult(LogOpsModel operation, MethodExecuteResultModel executeResult, Map<String, String> functionNameAndReturnMap) {
        String actionLog = operation.getFailureLogTpl();
        if (StringUtils.isBlank(actionLog)) {
            return;
        }

        // 获取全部表达式模板
        List<String> templates = getSpElTemplates(operation, actionLog);
        String operatorId = getOperatorIdFromService(operation, templates);
        Map<String, String> expressionValues = valueParser.processTemplate(templates, executeResult, functionNameAndReturnMap);
        // 日志持久化操作
        saveLog(expressionValues, operation, operatorId, actionLog, false);
    }


    private String getOperatorIdFromService(LogOpsModel operation, List<String> templates) {
        String finalOperatorId = "";
        if (StringUtils.isBlank(operation.getOperatorIdTpl()) && Objects.nonNull(operatorIdGetService)) {
            finalOperatorId = operatorIdGetService.getOperatorId();
            if (StringUtils.isBlank(finalOperatorId)) {
                throw new IllegalArgumentException("从 OperatorIdGetService 的实现类中获取 OperatorId 为空 ( Null / EMPTY_STRING)");
            }
        } else {
            templates.add(operation.getOperatorIdTpl());
        }
        return finalOperatorId;
    }


    private void saveLog(Map<String, String> expressionValues, LogOpsModel operation, String operatorId,
                         String actionLog, boolean isSuccess) {
        String action = expressionValues.get(actionLog);
        if (StringUtils.isBlank(action) || StringUtils.equalsIgnoreCase(action, actionLog)) {
            return;
        }
        OperationLogRecordModel recordModel = OperationLogRecordModel.builder()
                .type(expressionValues.get(operation.getBizTypeTpl()))
                .subType(expressionValues.get(operation.getBizSubTypeTpl()))
                .bizNo(expressionValues.get(operation.getBizIdTpl()))
                .operator(operatorId)
                .operation(expressionValues.get(actionLog))
                .createTime(new Date())
                .extra(expressionValues.get(operation.getExtraTpl()))
                .isSuccess(isSuccess)
                .build();
        logRecordService.record(recordModel);
    }


    /**
     * 通过对比方法签名，验证注解方法
     */
    private Method extractMethod(JoinPoint joinPoint) {
        Method method = null;
        try {
            Signature signature = joinPoint.getSignature();
            MethodSignature ms = (MethodSignature) signature;
            Object target = joinPoint.getTarget();
            method = target.getClass().getMethod(ms.getName(), ms.getParameterTypes());
        } catch (NoSuchMethodException e) {
            log.error("获取 OperationLog 注解方法异常", e);
        }
        return method;
    }


    private Class<?> getTargetClass(JoinPoint joinPoint) {
        Object target = joinPoint.getTarget();
        return AopProxyUtils.ultimateTargetClass(target);
    }


    private void logPostProcessing(LogDetailModel logDetailModel, Long finalExecutionCost) {
        // 日志后置处理
        if (Objects.nonNull(logPostProcessHandler)) {
            try {
                logDetailModel.setExecutionTime(finalExecutionCost);
                // todo 可以增加后置处理自动重试机制
                logPostProcessHandler.handle(logDetailModel);
            } catch (Throwable throwable) {
                log.error("日志切面执行后置处理时出现异常: ", throwable);
                if (Objects.nonNull(logRecordErrorHandler)) {
                    logRecordErrorHandler.handleLogPostProcessError();
                }
            }
        }
        // todo 后置处理完成后可以将日志通过某种渠道发送出去
    }

    @Override
    public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
        this.beanFactory = beanFactory;
    }

    @Autowired(required = false)
    public void setLogPostProcessHandler(IOperationLogPostProcessHandler logPostProcessHandler) {
        this.logPostProcessHandler = logPostProcessHandler;
    }

    @Autowired(required = false)
    public void setLogRecordErrorHandler(LogRecordErrorHandler logRecordErrorHandler) {
        this.logRecordErrorHandler = logRecordErrorHandler;
    }

    @Autowired(required = false)
    public void setOperatorIdGetService(IOperatorIdGetService operatorIdGetService) {
        this.operatorIdGetService = operatorIdGetService;
    }

    @Autowired(required = false)
    public void setLogRecordService(IOperationLogRecordService logRecordService) {
        this.logRecordService = logRecordService;
    }
}
