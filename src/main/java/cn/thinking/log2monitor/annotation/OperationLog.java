package cn.thinking.log2monitor.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 需要添加日志记录的方法注解
 *
 * @author Long.F
 * @date 2022/12/27 9:36
 */
@Documented
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Repeatable(OperationLogs.class)
public @interface OperationLog {

    /**
     * 业务唯一ID，操作日志绑定的业务标识
     * 必填
     * SpEL表达式
     */
    String bizId();

    /**
     * 操作日志的业务类型
     * 必填
     * SpEL表达式
     */
    String bizType();

    /**
     * 操作日志的业务子类型，比如订单的C端日志，和订单的B端日志，虽然都是订单类型，但是子类型不一样
     */
    String bizSubType() default "";

    /**
     * 日志自定义标签
     * 可选
     * SpEL表达式
     */
    String tag() default "";

    /**
     * 方法执行成功后的日志内容
     * 必填
     * SpEL表达式
     */
    String successMsg();

    /**
     * 方法执行失败后的日志内容
     * 可选
     * SpEL表达式
     */
    String failureMsg() default "";

    /**
     * 额外信息
     * 可选
     * SpEL表达式
     */
    String extra() default "";

    /**
     * 操作日志绑定的操作者标识
     * 可选
     * SpEL表达式
     */
    String operatorId() default "";

    /**
     * 是否记录方法执行后的返回值
     * true: 记录返回值
     * false: 不记录返回值
     */
    boolean recordReturnValue() default false;

    /**
     * 日志记录条件
     * 可选
     * SpEL表达式
     */
    String recordCondition() default "true";


    /**
     * 记录成功日志的条件
     *
     * @return 记录方法执行成功的日志的条件表达式，默认为空，代表默认都是记录成功的操作日志
     */
    String recordSuccessCondition() default "";

    /**
     * 自定义方法执行是否成功 用于根据返回体或其他情况下自定义日志实体中的success字段
     * 可选
     * SpEL表达式
     */
    String customizeSuccess() default "";

}
