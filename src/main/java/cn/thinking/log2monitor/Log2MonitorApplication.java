package cn.thinking.log2monitor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author Long
 */
@SpringBootApplication
public class Log2MonitorApplication {

    public static void main(String[] args) {
        SpringApplication.run(Log2MonitorApplication.class, args);
    }

}
