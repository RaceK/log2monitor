package cn.thinking.log2monitor.model;

import cn.thinking.log2monitor.annotation.OperationLog;
import lombok.Builder;
import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * {@link OperationLog} 注解解析后Model
 *
 * @author Long.F
 * @date 2022/12/27 10:23
 */
@Data
@Builder
public class LogDetailModel {

    /**
     * 日志唯一ID
     */
    private String logId;
    /**
     * 业务ID
     */
    private String bizId;
    /**
     * 业务类型
     */
    private String bizType;
    /**
     * 方法异常信息
     */
    private String exception;
    /**
     * 日志操作时间
     */
    private Date operateDate;
    /**
     * 方法是否成功  为NULL则表示没有自定义结果
     */
    private Boolean success;
    /**
     * 日志内容
     */
    private String msg;
    /**
     * 日志标签
     */
    private String tag;
    /**
     * 方法结果
     */
    private String returnStr;
    /**
     * 方法执行时间（单位：毫秒）
     */
    private Long executionTime;
    /**
     * 额外信息
     */
    private String extra;
    /**
     * 操作人ID
     */
    private String operatorId;
    /**
     * 实体DIFF列表
     */
    private List<DiffClazzModel> diffClazzModelList;

}
