package cn.thinking.log2monitor.model;

import lombok.Data;

/**
 *
 *
 * @author Long.F
 * @date 2022/12/27 10:53
 */
@Data
public class DiffFieldModel {

    /**
     * 字段名
     */
    private String fieldName;

    /**
     * 字段别名
     */
    private String oldFieldAlias;

    /**
     * 字段别名
     */
    private String newFieldAlias;

    /**
     * 旧值
     */
    private Object oldValue;

    /**
     * 新值
     */
    private Object newValue;

}
