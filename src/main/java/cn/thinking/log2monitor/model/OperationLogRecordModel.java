package cn.thinking.log2monitor.model;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;

/**
 * @author Long.F
 * @date 2023/1/8 15:40
 */
@Data
@Builder
public class OperationLogRecordModel implements Serializable {

    private Serializable id;

    private String type;
    private String subType;
    private String bizNo;
    private String operator;
    private boolean isSuccess;
    private Date createTime;
    private String operation;
    private String extra;
    private Map<String, String> features;


}
