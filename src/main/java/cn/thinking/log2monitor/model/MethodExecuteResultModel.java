package cn.thinking.log2monitor.model;

import lombok.Getter;

import java.lang.reflect.Method;

/**
 * 方法执行结果
 *
 * @author Long.F
 * @date 2022/12/29 15:23
 */
@Getter
public class MethodExecuteResultModel {

    /**
     * 方法执行是否成功
     */
    private boolean success = false;

    /**
     * 方法执行抛出的异常，无异常则为 Null
     */
    private Throwable throwable;

    /**
     * 执行异常后抛出的异常信息
     */
    private String errorMsg;

    /**
     * 方法执行结果
     */
    private Object result;

    /**
     * 执行的方法所属的类Class
     */
    private final Class<?> targetClass;

    /**
     * 执行的方法 Method
     */
    private final Method method;

    /**
     * 方法的参数
     */
    private final Object[] args;

    /**
     * 执行耗费的时间
     */
    private Long executeCost;


    public MethodExecuteResultModel(Method method, Object[] args, Class<?> targetClass) {
        this.method = method;
        this.args = args;
        this.targetClass = targetClass;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public void setThrowable(Throwable throwable) {
        this.throwable = throwable;
        this.errorMsg = throwable.getMessage();
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public void setResult(Object result) {
        this.result = result;
    }

    public void setExecuteCost(Long executeCost) {
        this.executeCost = executeCost;
    }
}
