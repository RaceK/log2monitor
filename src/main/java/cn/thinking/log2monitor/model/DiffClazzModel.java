package cn.thinking.log2monitor.model;

import lombok.Data;

import java.util.List;

/**
 *
 *
 * @author Long.F
 * @date 2022/12/27 10:54
 */
@Data
public class DiffClazzModel {

    /**
     * 实体类名
     */
    private String oldClassName;

    /**
     * 实体类别名
     */
    private String oldClassAlias;

    /**
     * 实体类名
     */
    private String newClassName;

    /**
     * 实体类别名
     */
    private String newClassAlias;

    /**
     * 字段Diff列表
     */
    private List<DiffFieldModel> diffFieldModelList;

}
