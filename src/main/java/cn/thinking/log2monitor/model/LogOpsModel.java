package cn.thinking.log2monitor.model;

import lombok.Builder;
import lombok.Data;

/**
 * 日志操作实体类  记录这注解解析之后需要对日志实体类进行的操作
 * tpl = template
 *
 * @author Long.F
 * @date 2022/12/30 9:38
 */
@Data
@Builder
public class LogOpsModel {
    private String successLogTpl;
    private String failureLogTpl;

    private String bizIdTpl;
    private String bizTypeTpl;
    private String bizSubTypeTpl;
    private String operatorIdTpl;
    private String tagTpl;
    private String extraTpl;
    private String recordConditionTpl;
    private String customizeSuccessTpl;
    private String recordSuccessCondition;

}
