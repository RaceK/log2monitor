package cn.thinking.log2monitor.context;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * 日志上下文，可以在上下文中存放变量以调用
 *
 * @author Long.F
 * @date 2022/12/27 11:25
 */
public class LogRecordContext {

    private LogRecordContext() {
        throw new IllegalStateException("该类为工具类，不可实例化");
    }

    /**
     * 局部变量
     * 考虑到可能会出现方法调用嵌套的情况，特地使用 Deque 模拟栈，来隔离嵌套调用时同名变量被覆盖的问题
     */
    private static final InheritableThreadLocal<Deque<Map<String, Object>>> LOCAL_VARIABLE_MAP_STACK = new InheritableThreadLocal<>();

    /**
     * 全局变量，不考虑变量之间的线程隔离情况
     * 如果同名会被覆盖
     */
    private static final InheritableThreadLocal<Map<String, Object>> GLOBAL_VARIABLE_MAP = new InheritableThreadLocal<>();


    /**
     * 放入一个局部变量
     */
    public static void putLocalVariable(String key, Object value) {
        if (Objects.isNull(LOCAL_VARIABLE_MAP_STACK.get())) {
            ArrayDeque<Map<String, Object>> stack = new ArrayDeque<>();
            LOCAL_VARIABLE_MAP_STACK.set(stack);
        }
        Deque<Map<String, Object>> stack = LOCAL_VARIABLE_MAP_STACK.get();
        if (stack.isEmpty()) {
            LOCAL_VARIABLE_MAP_STACK.get().push(new HashMap<>(16));
        }
        LOCAL_VARIABLE_MAP_STACK.get().element().put(key, value);
    }

    /**
     * 放入一个全局变量
     */
    public static void putGlobalVariable(String key, Object value) {
        if (Objects.isNull(GLOBAL_VARIABLE_MAP.get())) {
            GLOBAL_VARIABLE_MAP.set(new HashMap<>(16));
        }
        GLOBAL_VARIABLE_MAP.get().put(key, value);
    }

    public static Object getLocalVariable(String key) {
        Deque<Map<String, Object>> stack = LOCAL_VARIABLE_MAP_STACK.get();
        if (Objects.isNull(stack)) {
            return null;
        }
        Map<String, Object> firstMap = stack.peek();
        return Objects.isNull(firstMap) ? null : firstMap.get(key);
    }

    public static Object getGlobalVariable(String key) {
        Map<String, Object> cache = GLOBAL_VARIABLE_MAP.get();
        return Objects.isNull(cache) ? null : cache.get(key);
    }

    /**
     * 获取一个变量，默认从全局变量中获取，不存在则从局部变量中获取
     */
    public static Object getVariable(String key) {
        Object res = getGlobalVariable(key);
        if (Objects.nonNull(res)) {
            return res;
        }
        return getLocalVariable(key);
    }

    public static void clear() {
        if (LOCAL_VARIABLE_MAP_STACK.get() != null) {
            LOCAL_VARIABLE_MAP_STACK.get().pop();
        }
        if (LOCAL_VARIABLE_MAP_STACK.get().peek() == null) {
            GLOBAL_VARIABLE_MAP.remove();
        }
    }

    public static void putEmptyPlaceholder() {
        Deque<Map<String, Object>> stack = LOCAL_VARIABLE_MAP_STACK.get();
        if (Objects.isNull(stack)) {
             LOCAL_VARIABLE_MAP_STACK.set(new ArrayDeque<>());
        }
        LOCAL_VARIABLE_MAP_STACK.get().push(new HashMap<>(16));

        if (Objects.isNull(GLOBAL_VARIABLE_MAP.get())) {
            GLOBAL_VARIABLE_MAP.set(new HashMap<>(16));
        }
    }

}
