package cn.thinking.log2monitor.context;

import org.springframework.context.expression.MethodBasedEvaluationContext;
import org.springframework.core.ParameterNameDiscoverer;

import java.lang.reflect.Method;

/**
 * @author Long.F
 * @date 2023/1/4 15:13
 */
public class LogRecordEvaluationContext extends MethodBasedEvaluationContext {

    public LogRecordEvaluationContext(Object rootObject, Method method, Object[] arguments,
                                      ParameterNameDiscoverer parameterNameDiscoverer, Object returnRes, String errorMsg) {
        super(rootObject, method, arguments, parameterNameDiscoverer);

        setVariable("_return", returnRes);
        setVariable("_errorMsg", errorMsg);
    }

}
