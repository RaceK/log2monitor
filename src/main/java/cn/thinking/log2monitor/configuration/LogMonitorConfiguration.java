package cn.thinking.log2monitor.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author Long.F
 * @date 2022/12/27 16:35
 */
@Configuration
@ComponentScan("cn.thinking.log2monitor")
public class LogMonitorConfiguration {
}
