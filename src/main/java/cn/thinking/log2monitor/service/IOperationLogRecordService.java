package cn.thinking.log2monitor.service;

import cn.thinking.log2monitor.model.OperationLogRecordModel;

/**
 * 操作日志的记录Service
 *
 * @author Long.F
 * @date 2023/1/8 15:26
 */
public interface IOperationLogRecordService {

    /**
     * 记录操作日志
     *
     * @param recordModel 操作日志
     */
    void record(OperationLogRecordModel recordModel);

}
