package cn.thinking.log2monitor.service;

import cn.thinking.log2monitor.model.LogDetailModel;

/**
 * 日志后置处理
 *
 * @author Long.F
 * @date 2022/12/28 17:59
 */
public interface IOperationLogPostProcessHandler {

    /**
     * 自定义日志后置处理
     *
     * @param logDetailModel 日志详细信息Model
     * @throws Exception 处理器执行过程中抛出的一场
     */
    void handle(LogDetailModel logDetailModel) throws Exception;

}
