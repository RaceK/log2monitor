package cn.thinking.log2monitor.service;

/**
 * 日志处理失败/异常处理器
 *
 * @author Long.F
 * @date 2022/12/28 17:59
 */
public interface LogRecordErrorHandler {

    /**
     * 日志后置处理时出现异常后的异常兜底处理
     */
    default void handleLogPostProcessError() {}

}
