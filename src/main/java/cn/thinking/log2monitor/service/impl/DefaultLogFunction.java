package cn.thinking.log2monitor.service.impl;

import cn.thinking.log2monitor.service.ILogFunction;
import org.springframework.stereotype.Component;

/**
 * 默认的日志函数
 * {DEFAULT{#user.id}}
 *
 * @author Long.F
 * @date 2023/1/6 9:44
 */
@Component
public class DefaultLogFunction implements ILogFunction {

    @Override
    public boolean ifExecuteBefore() {
        return true;
    }

    @Override
    public String getFunctionName() {
        return "DEFAULT";
    }

    @Override
    public String apply(Object value) {
        return value.toString();
    }
}
