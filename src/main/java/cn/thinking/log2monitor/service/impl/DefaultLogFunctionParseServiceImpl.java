package cn.thinking.log2monitor.service.impl;

import cn.thinking.log2monitor.service.ILogFunction;
import cn.thinking.log2monitor.service.ILogFunctionParseService;
import org.springframework.stereotype.Component;

import java.util.Objects;

/**
 * 默认的日志函数解析Service实现类
 *
 * @author Long.F
 * @date 2023/1/5 18:10
 */
@Component
public class DefaultLogFunctionParseServiceImpl implements ILogFunctionParseService {

    LogFunctionFactory logFunctionFactory;

    public DefaultLogFunctionParseServiceImpl(LogFunctionFactory logFunctionFactory) {
        this.logFunctionFactory = logFunctionFactory;
    }

    @Override
    public String applyFunction(String functionName, Object value) {
        ILogFunction function = logFunctionFactory.getFunction(functionName);
        if (Objects.isNull(function)) {
            return value.toString();
        }
        return function.apply(value);

    }

    @Override
    public boolean isLogFunction(String functionName) {
        return logFunctionFactory.isBeforeFunction(functionName);
    }

}
