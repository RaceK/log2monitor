package cn.thinking.log2monitor.service.impl;

import cn.thinking.log2monitor.model.OperationLogRecordModel;
import cn.thinking.log2monitor.service.IOperationLogRecordService;
import com.alibaba.fastjson2.JSON;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @author Long.F
 * @date 2023/1/11 17:36
 */
@Slf4j
@Component
public class DefaultOperationLogRecordServiceImpl implements IOperationLogRecordService {


    @Override
    public void record(OperationLogRecordModel recordModel) {
        log.info(JSON.toJSONString(recordModel));
    }

}
