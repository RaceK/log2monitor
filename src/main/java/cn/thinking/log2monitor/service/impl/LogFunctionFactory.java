package cn.thinking.log2monitor.service.impl;

import cn.thinking.log2monitor.service.ILogFunction;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 日志函数工厂
 *
 * @author Long.F
 * @date 2023/1/5 18:11
 */
@Component
public class LogFunctionFactory {

    private Map<String, ILogFunction> allFunctionMap;

    public LogFunctionFactory(List<ILogFunction> parseFunctions) {
        if (CollectionUtils.isEmpty(parseFunctions)) {
            return;
        }
        allFunctionMap = new HashMap<>();
        for (ILogFunction parseFunction : parseFunctions) {
            if (StringUtils.isBlank(parseFunction.getFunctionName())) {
                continue;
            }
            allFunctionMap.put(parseFunction.getFunctionName(), parseFunction);
        }
    }

    public ILogFunction getFunction(String functionName) {
        return allFunctionMap.get(functionName);
    }


    public boolean isBeforeFunction(String functionName) {
        return allFunctionMap.get(functionName) != null && allFunctionMap.get(functionName).ifExecuteBefore();
    }

}
