package cn.thinking.log2monitor.service.impl;

import cn.thinking.log2monitor.service.ILogPerformanceMonitor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;

/**
 * @author Long.F
 * @date 2023/1/7 15:46
 */
@Slf4j
@Component
public class DefaultLogPerformanceMonitor implements ILogPerformanceMonitor {


    @Override
    public void output(StopWatch stopWatch) {
        log.info("操作日志记录性能记录如下：\n{}", stopWatch.prettyPrint());
    }


}
