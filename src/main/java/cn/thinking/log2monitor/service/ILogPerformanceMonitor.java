package cn.thinking.log2monitor.service;

import org.springframework.util.StopWatch;

/**
 * 性能监视
 *
 * @author Long.F
 * @date 2023/01/07 15:30
 */
public interface ILogPerformanceMonitor {

    String MONITOR_NAME = "log-performance";
    String MONITOR_TASK_BEFORE_EXECUTE = "before-execute";
    String MONITOR_TASK_EXECUTE = "execute";
    String MONITOR_TASK_AFTER_EXECUTE = "after-execute";

    /**
     * 监视方法
     * @param stopWatch 内部通过StopWatch记录耗时
     */
    void output(StopWatch stopWatch);



}
