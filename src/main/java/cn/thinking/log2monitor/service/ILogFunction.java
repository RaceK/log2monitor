package cn.thinking.log2monitor.service;

/**
 * 日志函数定义接口
 *
 * @author Long.F
 * @date 2023/1/5 18:11
 */
public interface ILogFunction {

    /**
     * 是否在执行目标方法之前解析 默认是 false
     * @return true if yes
     */
    default boolean ifExecuteBefore() {
        return false;
    }

    /**
     * 函数的名称
     * 例如 {_DIFF{#oldUser.id, #newUser.id}} 则 _DIFF 就是函数的名称
     *
     * @return 函数的名称
     */
    String getFunctionName();

    /**
     * 执行日志函数的解析流程
     *
     * @param value 函数入参
     * @return 文案
     */
    String apply(Object value);

}
