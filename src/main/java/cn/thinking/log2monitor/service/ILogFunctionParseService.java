package cn.thinking.log2monitor.service;

/**
 * 日志函数解析 Service
 *
 * @author Long.F
 * @date 2023/1/5 18:09
 */
public interface ILogFunctionParseService {

    /**
     * 执行日志函数
     *
     * @param functionName 函数名称
     * @param functionArgs 函数入参
     * @return 函数执行结果
     */
    String applyFunction(String functionName, Object functionArgs);

    /**
     * 判断是否是日志函数
     *
     * @param functionName 函数名称
     * @return true if yes
     */
    boolean isLogFunction(String functionName);

}
