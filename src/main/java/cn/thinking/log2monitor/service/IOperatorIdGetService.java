package cn.thinking.log2monitor.service;

/**
 * 自定义获取 OperatorId 的处理器
 *
 * @author Long.F
 * @date 2022/12/27 11:39
 */
public interface IOperatorIdGetService {

    /**
     * 获取操作者ID
     * @return 操作者ID
     */
    String getOperatorId();

}
